import numpy as np
import pickle
from datetime import tzinfo, timedelta, datetime
from bokeh.layouts import gridplot
from bokeh.plotting import figure, show, output_file
from bokeh.sampledata.stocks import AAPL, GOOG, IBM, MSFT
from bokeh.charts import Step
from bokeh.models import LinearAxis, Range1d

#This does overlay plot


class FixedOffset(tzinfo):
    """Fixed offset in minutes east from UTC."""

    def __init__(self, offset, name):
        self.__offset = timedelta(minutes = offset)
        self.__name = name

    def utcoffset(self, dt):
        return self.__offset

    def tzname(self, dt):
        return self.__name

    def dst(self, dt):
        return ZERO

f = open('../results/P15_PerfMon.csv', 'r')
l = f.readline()
l = f.readline()
perfMon = {'timestamp':[],'anonUsers':[]}

while l:
	v = l.split(",")
	if len(v) >= 13:
		timeStr = v[0].replace("\"",'')
		myTime = datetime.strptime(timeStr, "%m/%d/%Y %H:%M:%S.%f") + timedelta(hours=7)
		#myTime.replace(tzinfo=FixedOffset(-7, 'PDT'))
		val = float(v[12].replace("\"",''))
		perfMon['timestamp'].append((myTime - datetime(1970,1,1)).total_seconds())
		perfMon['anonUsers'].append(val)
	l = f.readline()
f.close()


f = open('../results/P15_LocustCustom_queries only slow climb_U11Mx2000Mn1000H0.05_results.pkl', 'rb')
results = pickle.load(f)
f.close()
print len(results['req_errors'])

throughput = {'time':[], 'rate':[]}
for r in results['throughput']:
	throughput['time'].append(r['time'])
	throughput['rate'].append(r['rate'])

req_success = {'timestamp':[],'req_len':[],'req_duration':[]}
for r in results['req_success']:
	req_success['req_duration'].append(r['time'])
	req_success['req_len'].append(r['len'])
	req_success['timestamp'].append(r['timestamp'])


s1 = figure(plot_width=1000, plot_height=600, y_axis_label='Request Duration')
s1.circle(req_success['timestamp'],req_success['req_duration'], size=4, legend='Request Duration', color='darkgrey', alpha=0.2)

s1.extra_y_ranges = {"User Count": Range1d(start=0, end= max(perfMon['anonUsers']))}
s1.line(perfMon['timestamp'],perfMon['anonUsers'], legend="Anon Users", y_range_name='User Count')
s1.add_layout(LinearAxis(y_range_name="User Count", axis_label='Anon Users'), 'left')

s1.legend.click_policy="hide"

# times2 = []
# dls = []
# for r in results['downloads']:
# 	times2.append(r['time'])
# 	dls.append(r['downloads'])

# p2 = figure(x_axis_type="datetime", title="Concurrent Downloads")
# p2.xaxis.axis_label = 'Time'
# p2.yaxis.axis_label = 'Number of Concurrent Downloads'
# p2.line(, dls)
# p2.circle(aapl_dates, aapl, size=4, legend='close',
#           color='darkgrey', alpha=0.2)

# p2.line(aapl_dates, aapl_avg, legend='avg', color='navy')
# p2.legend.location = "top_left"
print "Will save"
output_file("results.html", title="Load Test Measurements")

show(s1)  # open a browser