from locust import events
import time
try:
   import cPickle as pickle
except:
   import pickle

metrics = {
			'req_success':[],
			'throughput':[],
			'downloads':[],
			'req_errors':[]
		  }
downloads=0

reqError = events.EventHook()
def req_error(**kw):
	metrics['req_errors'].append({'timestamp':time.time(), 'msg':kw.get('message','')})
	print "Req Error: %s"%kw.get("message","unknown")
reqError += req_error

def req_success(request_type, name, response_time, response_length, **kw):
	#print "hello"
	metrics['req_success'].append({'time':response_time, 'len':response_length, 'timestamp':time.time()})
events.request_success += req_success

downloadStart = events.EventHook()
def on_download_start(**kw):
	global downloads, metrics
	size = kw.get('size',-1)
	downloads += 1
	print "Downlod Started: %d"%downloads
	metrics['downloads'].append({ 'time':time.time(), 'downloads':downloads})
downloadStart += on_download_start

downloadEnd = events.EventHook()
def on_download_end(**kw):
	global downloads, metrics
	size = kw.get('size',-1)
	downloads -=1
	print "Download Ended: %d"%downloads
	metrics['downloads'].append({'time':time.time(), 'downloads':downloads})

downloadEnd += on_download_end

chunkHook = events.EventHook()
bytesRecvd = 0
timeSpent = 0.0
def on_chunk(**kw):
	global bytesRecvd, timeSpent, metrics, downloads
	bytesRecvd += kw.get('bytesRead',0)
	timeSpent += kw.get('seconds', 0.0)
	rate = 0
	if timeSpent > 0:
		rate= bytesRecvd/timeSpent
	metrics['throughput'].append({'time':time.time(), 'len':bytesRecvd, 'timeSpent':timeSpent, 'downloads':downloads, 'rate':rate})	
chunkHook += on_chunk

def exiting():
	# pass
	global metrics
	tfin = int(time.time())
	f = open('../results/%d_results.pkl'%tfin, 'wb')
	pickle.dump(metrics, f)
	f.flush()
	f.close()
	print "Req metrics captured: %d"%len(metrics)




events.quitting += exiting