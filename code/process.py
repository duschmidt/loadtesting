import numpy as np
import pickle
from datetime import tzinfo, timedelta, datetime
from bokeh.layouts import gridplot
from bokeh.plotting import figure, show, output_file
from bokeh.sampledata.stocks import AAPL, GOOG, IBM, MSFT
from bokeh.charts import Step
from bokeh.models import Span



class FixedOffset(tzinfo):
    """Fixed offset in minutes east from UTC."""

    def __init__(self, offset, name):
        self.__offset = timedelta(minutes = offset)
        self.__name = name

    def utcoffset(self, dt):
        return self.__offset

    def tzname(self, dt):
        return self.__name

    def dst(self, dt):
        return ZERO

f = open('../results/P15_PerfMon.csv', 'r')
l = f.readline()
l = f.readline()
perfMonTimes = []
anonUsers = []
while l:
	v = l.split(",")
	if len(v) >= 13:
		timeStr = v[0].replace("\"",'')
		myTime = datetime.strptime(timeStr, "%m/%d/%Y %H:%M:%S.%f")
		myTime.replace(tzinfo=FixedOffset(-7, 'PDT'))
		val = float(v[12].replace("\"",''))
		perfMonTimes.append((myTime - datetime(1970,1,1)).total_seconds())
		anonUsers.append(val)
		l = f.readline()
f.close()

p5 = figure()
p5.line(perfMonTimes, anonUsers)





def np_datetime(x):
	return x
    #return np.array(x, dtype=np.datetime64)

f = open('../results/P15_LocustCustom_U11Mx2000Mn1000H0.05_results.pkl', 'rb')
results = pickle.load(f)
print len(results['req_errors'])

times = []
rates = []
for r in results['throughput']:
	times.append(r['time'])
	rates.append(r['rate'])


# p1 = figure(x_axis_type="datetime", title="Throughput")
# p1.grid.grid_line_alpha=0.3
# p1.xaxis.axis_label = 'Time'
# p1.yaxis.axis_label = 'Transfer Rate Bytes/sec'

# p1.line(np_datetime(times), rates, color='#A6CEE3', legend='Throughput Bytes/sec')

#p2 = Step(results['downloads'],y=['downloads'],x='time')

times2 = []
lens = []
for r in results['req_success']:
	times2.append(r['time'])
	lens.append(r['len'])

p3 = figure(x_axis_type="datetime", title="request rates")
p3.circle(np_datetime(times2), lens)



times3 = []
lens = []
for r in results['req_success']:
	times3.append(r['timestamp'])
	lens.append(r['time'])

p4 = figure(x_axis_type="datetime", title="request rates")
p4.circle(np_datetime(times3), lens)
# times2 = []
# dls = []
# for r in results['downloads']:
# 	times2.append(r['time'])
# 	dls.append(r['downloads'])

# p2 = figure(x_axis_type="datetime", title="Concurrent Downloads")
# p2.xaxis.axis_label = 'Time'
# p2.yaxis.axis_label = 'Number of Concurrent Downloads'
# p2.line(times2, dls)
# p2.circle(aapl_dates, aapl, size=4, legend='close',
#           color='darkgrey', alpha=0.2)

# p2.line(aapl_dates, aapl_avg, legend='avg', color='navy')
# p2.legend.location = "top_left"
print "Will save"
output_file("results.html", title="stocks.py example")

show(gridplot([[p3,p4]], plot_width=400, plot_height=400))  # open a browser