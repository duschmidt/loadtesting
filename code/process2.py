import numpy as np
import pickle
from datetime import tzinfo, timedelta, datetime
from bokeh.layouts import gridplot
from bokeh.plotting import figure, show, output_file
from bokeh.sampledata.stocks import AAPL, GOOG, IBM, MSFT
from bokeh.charts import Step
from bokeh.models import Span

f = open('../results/1496774562_results.pkl', 'rb')
results = pickle.load(f)
