from locust import HttpLocust, TaskSet, task
import random
import json
import locustEvents
import time
import webui
import resource
resource.setrlimit(resource.RLIMIT_NOFILE, (999999, 999999))

class UserBehavior(TaskSet):

    extent=None

    def on_start(self):
        if self.extent is None:
            print "Grab Extent"
            with self.client.post('/arcgis/rest/services/Public/LDQ_Status_newViewer/MapServer/0/',{'f':'json'}, catch_response=True) as response:
                try:
                    params = json.loads(response.content)
                except:
                    locustEvents.reqError.fire(message='Bad Response Content - Definition')
                    response.failure('Could not parse json')
                    return

                self.extent = params['extent']

    @task
    def queryPointWithGeom(self):
        # params = None
        # with self.client.post('/arcgis/rest/services/Public/LDQ_Status_newViewer/MapServer/0/',{'f':'json'}, catch_response=True) as response:
        #     try:
        #         params = json.loads(response.content)
        #     except:
        #         locustEvents.reqError.fire(message='Bad Response Content - Definition')
        #         response.failure('Could not parse json')
        #         return

        
        x1 = random.uniform(self.extent['xmin'], self.extent['xmax'])
        x2 = random.uniform(x1, self.extent['xmax'])
        y1 = random.uniform(self.extent['ymin'], self.extent['ymax'])
        y2 = y1 + (x2-x1) #create a square envelope
        geom = {"xmin":x1,"ymin":y1,"xmax":x2,"ymax":y2,"spatialReference":{"wkid":102100}}
        #print geom
        #geom = {"xmin":-13071343.332988407,"ymin":5400734.670511454,"xmax":-12993071.816024356,"ymax":5479006.187475506,"spatialReference":{"wkid":102100}}
        queryData = {
            'f':'json',
            'geometry':geom, 
            'geometryType':'esriGeometryEnvelope', 
            'returnGeometry':'true', 
            'spatialRel':'esriSpatialRelIntersects', 
            'outFields':'*',
            'inSR':102100,
            'outSR':102100
        }
        # queryData = {'f':'json',
        #     'returnGeometry':'true',
        #     'spatialRel':'esriSpatialRelIntersects',
        #     'maxAllowableOffset':305,
        #     'geometry':{"xmin":-13462700.917814326,"ymin":5635549.221416015,"xmax":-13306157.88388636,"ymax":5792092.255343981,"spatialReference":{"wkid":102100}},
        #     'geometryType':'esriGeometryEnvelope',
        #     'inSR':102100,
        #     'outFields':'*',
        #     'outSR':102100}
        #print queryData['geometry']
        qstr = "?"
        val = None
        for k,v in queryData.items():
            if k =='geom':
                val = json.dumps(geom)
            else:
                val = v
            qstr += "%s=%s&"%(k, str(val))
        qstr = qstr[0:-1]
        self.client.get('/arcgis/rest/services/Public/LDQ_Status_newViewer/MapServer/0/query'+qstr, name="LDQ_Status_newViewer/MapServer/0/query envelope")
        # with self.client.post('/arcgis/rest/services/Public/LDQ_Status_newViewer/MapServer/0/query/',queryData, catch_response=True) as query:
        #     try:
        #         if len(query.content) == 0:
        #             raise Exception()
        #         result = json.loads(query.content)
        #         print len(query.content)
        #     except:
        #         locustEvents.reqError.fire(message='Bad Response Content - Query point with geom')
        #res = json.loads(query.content)
        #print res.keys()
        #import pdb; pdb.set_trace()

    #@task(100)
    # def queryPointNoGeom(self):
    #     params = None
    #     with self.client.post('/arcgis/rest/services/Public/LDQ_Status_newViewer/MapServer/0/',{'f':'json'}, catch_response=True) as response:
    #         try:
    #             params = json.loads(response.content)
    #         except:
    #             locustEvents.reqError.fire(message='Bad Response Content - Definition')
    #             response.failure('Could not parse json')
    #             return
    #     xcoord = random.uniform(params['extent']['xmin'], params['extent']['xmax'])
    #     ycoord = random.uniform(params['extent']['ymin'], params['extent']['ymax'])
    #     queryData = {'f':'pjson','geometry':{'x':xcoord, 'y':ycoord}, 'geometryType':'esriGeometryPoint', 'returnGeometry':'false', 'spatialRel':'esriSpatialRelWithin', 'outFields':'*'}
    #     with self.client.post('/arcgis/rest/services/Public/LDQ_Status_newViewer/MapServer/0/query/',queryData, catch_response=True) as query:
    #         try:
    #             result = json.loads(query.content)
    #         except:
    #             locustEvents.reqError.fire(message='Bad Response Content - Query point without geom')
    #     #res = json.loads(query.content)
    #     #print res.keys()
    #     #import pdb; pdb.set_trace()

    #@task(1)
    def downloadFile(self):
        params = None
        with self.client.post('/arcgis/rest/services/Public/LDQ_Status_newViewer/MapServer/0/',{'f':'json'}, catch_response=True) as response:
            try:
                params = json.loads(response.content)
            except:
                locustEvents.reqError.fire(message='Bad Response Content - Definition')
                response.failure('Could not parse json')
                return
        xcoord = random.uniform(self.extent['xmin'], self.extent['xmax'])
        ycoord = random.uniform(self.extent['ymin'], self.extent['ymax'])
        queryData = {
            'f':'json',
            'geometry':{'x':xcoord, 'y':ycoord,"spatialReference":{"wkid":102100}}, 
            'geometryType':'esriGeometryPoint', 
            'returnGeometry':'true', 
            'spatialRel':'esriSpatialRelIntersects', 
            'outFields':'*',
            'inSR':102100,
            'outSR':102100
        }

        qstr = "?"
        val = None
        for k,v in queryData.items():
            if k =='geom':
                val = json.dumps(geom)
            else:
                val = v
            qstr += "%s=%s&"%(k, str(val))
        qstr = qstr[0:-1]
        with self.client.get('/arcgis/rest/services/Public/LDQ_Status_newViewer/MapServer/0/query'+qstr, catch_response=True, name="LDQ_Status_newViewer/MapServer/0/query point") as query:
            res = None
            try:
                res = json.loads(query.content)
            except:
                locustEvents.reqError.fire(message='Bad Response Content - Query point without geom')
                return
            if res.has_key('features'):
                if  len(res['features']) == 0:
                    return
                url = res['features'][0]['attributes']['LDQ_zip']
                locustEvents.downloadStart.fire()
                response = self.client.request('get',url, stream=True)
                t0 = time.time()
                bytesRead = [0]
                times = [t0]
                totBytes = 0
                tl = t0
                for chunk in response.iter_content(chunk_size=1048576, decode_unicode=False):
                    l = len(chunk)
                    t = time.time()
                    totBytes += l
                    bytesRead.append(l)
                    times.append(t)
                    locustEvents.chunkHook.fire(bytesRead=l, seconds=t-tl)
                    tl = t

                seconds = times[-1] - times[0]
                sumBytes = sum(bytesRead)
                locustEvents.downloadEnd.fire()

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    #min_wait = 110000 # low freq oscillations in user count
    #max_wait = 120000 # 
    #min_wait = 8000  # 8-10 holds stead around correct number, some oscillations
    #max_wait = 10000
    # 8-8.1 holds a little high. web ui is severly delayed but did respond evenutally
    min_wait = 1000 
    max_wait = 2000

#notes:
# 10000-15000 and 30 users, no dls seems ok, user count too low, but fairly steady, minor higher freq oscillations
# Tried out 110000-120000 and 30 users but it made low freq oscillations